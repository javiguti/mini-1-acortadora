#!/usr/bin/python3


from urllib import parse

import webapp
from robot import Robot
import string
import random

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Resource: </label>
        <input type="text" name="resource" required>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

URL = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Lista de urls:</p> {content}
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Resource not found: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""


class RandomShort(webapp.webApp):

    def __init__(self, hostname, port):
        self.contents = {}
        self.urls = {}
        super().__init__(hostname, port)

    def parse(self, request):
        """Return the method name and resource name"""

        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            data['body'] = None
        else:
            data['body'] = request[body_start+4:]
        parts = request.split(' ', 2)
        data['method'] = parts[0]
        data['resource'] = parts[1]
        return data

    def process(self, data):
        """Produce the page with the content for the resource"""

        if data['method'] == 'GET':
            code, page = self.get(data['resource'])
        elif data['method'] == 'PUT':
            code, page = self.put(data['resource'], data['body'])
        elif data['method'] == 'POST':
            code, page = self.post(data['resource'], data['body'])
        else:
            code, page = "405 Method not allowed",\
                         PAGE_NOT_ALLOWED.format(method=data['method'])
        return code, page

    def get(self, resource):
        resource = resource[1:]
        if resource in self.contents.keys():
            content = Robot(self.contents[resource])
            content.retrieve()
            page = URL.format(content=content.content, resource=resource)
            code = "HTTP REDIRECT"
        else:
            page = PAGE_NOT_FOUND.format(resource=resource, form=FORM)
            code = "404 HTTP ERROR Recurso no disponible"
        return code, page

    def put(self, resource, body):
        self.contents[resource] = body
        page = PAGE.format(content=body, resource=resource, form=FORM)
        code = "200 OK"
        return code, page

    def post(self, resource, body):
        fields = parse.parse_qs(body)
        print("Fields:", fields)
        if resource == '/':
            if 'resource' in fields:
                resource = fields['resource'][0]
                if not (('https://' in resource) or ('http://' in resource)):
                    resource = 'https://' + resource
                if not (resource in self.contents.values()):
                    num_string = 5
                    string_length = 8
                    index = ''
                    for x in range(num_string):
                        index = ''.join(random.choice(string.ascii_letters + string.digits)
                                        for _ in range(string_length))
                    self.contents[index] = resource
                    page = PAGE.format(content=self.contents, form=FORM)
                    code = "200 OK"
                else:
                    page = PAGE.format(content=self.contents, form=FORM)
                    code = "200 OK"
            else:
                page = PAGE_UNPROCESABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            code = "405 Method not allowed"
            page = PAGE_NOT_ALLOWED.format(method='POST')
        return code, page


if __name__ == "__main__":
    webApp = RandomShort("localhost", 1234)
